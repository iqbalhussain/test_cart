/* Variables */
var plans = [
    {
        "id" : 1,
        "duration" : "1 MONTH PLAN",
        "active" : 0,
        "billing_cycle" : "monthly",
        "divider" : 1,
        "usd_old_price" : 10.95,
        "usd_price": 10.95,
        "eur_price": 10.95,
        "eur_old_price" : 10.95,
        "plan_text" : "1 Month Plan"
    },
    {
        "id" : 2,
        "duration" : "1 YEAR PLAN",
        "active" : 1,
        "billing_cycle" : "annually",
        "divider" : 12,
        "usd_old_price" : 10.95,
        "usd_price": 3.33,
        "eur_price": 3.33,
        "eur_old_price" : 10.95,
        "plan_text" : "1 Year Plan"
    },
    {
        "id": 3,
        "duration" : "3 MONTH PLAN",
        "active" : 0,
        "billing_cycle" : "triennially",
        "divider" : 3,
        "usd_old_price" : 10.95,
        "usd_price": 8.00,
        "eur_price": 8.00,
        "eur_old_price" : 10.95,
        "plan_text" : "3 Month Plan"
    }
];

var curencies = [ "USD","EUR" ];
var currency = "";
var stripe = {};
var elements = {};
var style = {};
var card = {};

$(document).ready(function(){

    /* Get First Active plan ID */
    var perv_plan_id = plans.filter( function(x) { return x.active === 1 }).map(function(x) { return x.id });
    var gateway = $(".payment-details");
    var plan_box = $(".payment-plan");
    var discount_box = $(".payment-plan-discount");
    var total_price_box = $(".payment-total");
    var discount_applied_box = $(".payment-plan-discount-coupon");
    var coupon_box = $(".payment-coupon-box");
    var coupon_box_message_box = $(".payment-coupon-box-message");

    /*
    * Selected Currency
    * */
    var currency_active = $(".currency-select-box li.active");
    if(typeof currency_active != 'undefined' && currency_active != null){
        currency = currency_active.text();
    }


    /*
    * Get active plan total price
    * */
    var active_plan_total_price = plans.filter(function (x){
        return x.active === 1;
    } ).map(function (x) {
        if(currency === 'USD'){
            return (x.usd_price * x.divider).toFixed(2);
        }
        else if(currency === 'EUR'){
            return (x.eur_price * x.divider).toFixed(2);
        }
    });

    var active_plan_id = $(".cart-box.active").data('pkg-id');

    $('.cart-box .cart-box-inner').on('click',function(){
        var cart_box = $(this).parent();
        var cart_id = cart_box.data('pkg-id');

        /*
         *
         * make cart box active
         *
         * */
        if(cart_box.length > 0 && !cart_box.hasClass('active')){
            $(".cart-box").removeClass('active');
            cart_box.addClass('active');
        }

        /*
         *
         * update gateway text and values
         *
         * */


        if(cart_id){

            if(cart_id !== perv_plan_id){
                discount_applied_box.removeClass('active');
                discount_applied_box.children('span').first().text('');
                discount_applied_box.children('span').last().text("");
                coupon_box.removeClass('active coupon-applied');
                $(".payment-coupon-inpt").val('');
                $(".payment-details .payment-coupon-box-message").removeClass('active success-msg error');
                $(".payment-details .payment-coupon span").removeClass('hidden');
            }

            /* Loop through all plans */
            plans.filter(function(x) { return x.id === cart_id }).map(function(x){
                var plan_price = 0;

                /* update plan box text */
                if(plan_box){
                    plan_box.children('span').first().text("PureVPN " + x.plan_text);
                    if(currency === 'USD'){
                        plan_price = (x.usd_old_price * x.divider).toFixed(2);
                    }
                    else if(currency === 'EUR'){
                        plan_price = (x.eur_old_price * x.divider).toFixed(2);
                    }
                    plan_box.children('span').last().text(currency + " " + plan_price);
                }

                /* update discount if exist */
                if(discount_box){
                    var discount_price = 0;
                    if(currency === 'USD'){
                        discount_price = ((x.usd_old_price * x.divider) - (x.usd_price * x.divider));
                    }
                    else if(currency === 'EUR'){
                        discount_price = ((x.eur_old_price * x.divider) - (x.usd_price * x.divider));
                    }
                    var discount_percentage = 0;

                    if(discount_price !== 0 && discount_price > 0){
                        discount_box.removeClass('hidden');
                        if(currency === 'USD'){
                            discount_percentage = Math.ceil((((x.usd_old_price * x.divider) - (x.usd_price * x.divider))/(x.usd_old_price * x.divider))*100 ) ;
                        }
                        else if(currency === 'EUR'){
                            discount_percentage = Math.ceil((((x.eur_old_price * x.divider) - (x.eur_price * x.divider))/(x.eur_old_price * x.divider))*100 ) ;
                        }
                    }else{
                        discount_box.addClass('hidden');
                    }
                    if(discount_price !== 0 && discount_percentage !== 0 && discount_price > 0){
                        discount_box.children('span').first().text("Discount " + discount_percentage + "%");
                        discount_box.children('span').last().text("- " + currency + " " + discount_price.toFixed(2));
                    }
                }

                /* update total price plan */
                if(total_price_box && plan_price !== 0){
                    var total_price = (plan_price - discount_price).toFixed(2);
                    total_price_box.children('span').last().text(currency + " " + total_price);
                }

            });

            /*
             *
             * Scroll to gateways
             *
             * */
            if($(".gateway-wrapper").length > 0){
                var gateway_wrapper = $(".gateway-wrapper");
                $('html, body').animate({
                    scrollTop: gateway_wrapper.offset().top
                }, 800);
            }

        }


    });

    /*
     *
     * Click on Coupon
     *
     * */
    $('.payment-details .payment-coupon span').on('click',function(){
        var got_coupon_btn = $(this);
        if(coupon_box){
            if(!coupon_box.hasClass('active')){
                coupon_box.addClass('active');
                got_coupon_btn.addClass('hidden');
            }
        }
    });

    /*
     *
     * Apply Coupon Code
     *
     * */
    $('.payment-coupon .payment-coupon-btn').on('click',function(){
        var coupon_apply_btn = $(this);
        var coupon_cancel_btn = $(this).siblings(".payment-coupon-cancel");
        var coupon_input = $(".payment-coupon-inpt");
        var coupon_message_box = $(".payment-coupon-box-message");
        var test_coupon = 'discount5';
        var test_coupon_discount = "05.00";
        if(coupon_input){
            if(coupon_input.val() !== ''){
                coupon_cancel_btn.hide();
                coupon_message_box.removeClass('active');
                coupon_apply_btn.parent().addClass('show-input-loader');

                setTimeout(function () {
                    coupon_message_box.addClass('active');
                    if(coupon_input.val() === test_coupon){
                        coupon_message_box.removeClass('error');
                        coupon_apply_btn.parent().addClass('coupon-applied');
                        coupon_message_box.addClass('success-msg');
                        coupon_input.prop('disabled',true);
                        coupon_message_box.children('span').text('Coupon code applied');
                        discount_applied_box.addClass('active');
                        coupon_apply_btn.addClass('disabled');
                        discount_applied_box.children('span').first().text('Coupon discount - USD 5.00');
                        discount_applied_box.children('span').last().text("- " + currency + " " + test_coupon_discount);
                        if($(".cart-box.active").data('pkg-id') !== perv_plan_id[0]){
                            plans.filter(function(x) { return x.id === $(".cart-box.active").data('pkg-id') }).map(function(x){
                                var updated_total_price = (((x.usd_old_price * x.divider) - ((x.usd_old_price * x.divider) - (x.usd_price * x.divider))).toFixed(2) - parseInt(test_coupon_discount));
                                total_price_box.children('span').last().text(currency + " " + updated_total_price.toFixed(2));
                            });
                        }else {
                            total_price_box.children('span').last().text(currency + " " + (active_plan_total_price - parseInt(test_coupon_discount)));
                        }
                    }else{
                        coupon_input.prop('disabled',false);
                        discount_applied_box.removeClass('active');
                        coupon_apply_btn.parent().removeClass('coupon-applied');
                        coupon_message_box.removeClass('success-msg');
                        coupon_message_box.addClass('error');
                        coupon_message_box.children('span').text('Coupon code invalid');
                        discount_applied_box.children('span').first().text('');
                        discount_applied_box.children('span').last().text("");
                    }
                    coupon_apply_btn.parent().removeClass('show-input-loader');
                    coupon_cancel_btn.show();
                    coupon_message_box.addClass('active');
                }, 2000);
            }
        }
    });

    /*
     *
     * Coupon Code Cancel
     *
     * */
    $('.payment-coupon-cancel').on('click', function(){
        $(this).hide();
        var _getCouponVal = $('.payment-coupon-inpt');
        _getCouponVal.prop('disabled',false);
        if ( _getCouponVal.val() !== '') {
            _getCouponVal.val('');
            discount_applied_box.removeClass('active');
            discount_applied_box.removeClass('disabled');
            $('.payment-coupon-box').removeClass('coupon-applied');
            $('.payment-coupon-btn').removeClass('focus-coupon');
            $('.payment-coupon-btn').addClass('disabled');
            $(".payment-details .payment-coupon-box-message").removeClass('active success-msg error');
            if($(".cart-box.active").data('pkg-id') !== perv_plan_id[0]){
                plans.filter(function(x) { return x.id === $(".cart-box.active").data('pkg-id') }).map(function(x){
                    var updated_total_price = (((x.usd_old_price * x.divider) - ((x.usd_old_price * x.divider) - (x.usd_price * x.divider))).toFixed(2));
                    // console.log(updated_total_price);
                    total_price_box.children('span').last().text(currency + " " + updated_total_price);
                });
            }else {
                total_price_box.children('span').last().text(currency + " " + (active_plan_total_price));
            }
        }
    });

    /*
     *
     * Coupon Code Input Focus
     *
     * */
    $(document).on('keyup', '.payment-coupon-inpt', function(){
        coupon_box_message_box.removeClass('active');
        var _getCouponBtn = $('.payment-coupon-btn');
        var _getCouponCancelBtn = $(this).siblings('.payment-coupon-cancel');
        if ( $(this).val() !== '') {
            _getCouponCancelBtn.show();
            _getCouponBtn.removeClass('disabled');
            _getCouponBtn.addClass('focus-coupon')
        }else{
            _getCouponCancelBtn.hide();
            _getCouponBtn.addClass('disabled');
            _getCouponBtn.removeClass('focus-coupon')
        }
    });


    $('.gateway-method').on('click',function(){
        var cart_box = $(this);
        var gateway_id = $(this).attr("id");
        var gateway_name = $(this).data("name");
        if(cart_box.length > 0 && !cart_box.hasClass('active')){
            $(".gateway-method").removeClass('active');
            cart_box.addClass('active');
            if(gateway_id !== 'stripe'){
                $("#creditCardContainer,#cardHolderNameContainer").hide();
            }else {
                $("#creditCardContainer,#cardHolderNameContainer").show();
            }
            $("#submit_btn_title").text(gateway_name);
            $(".payment-details").removeClass('submitted');
            $('.payment.success').hide();
            $("#agree_with_pay").html(gateway_name);
            // updateCreditCardFields(gateway_name);
        }
    });


    $('#card-element').bind('keydown',function (e) {
        if( e.which == 9 ) {
            $("button.payment-btn").focus();
            e.preventDefault();
        }
    });

    focusOutEmailField();

});

// $(".currency-select-box ul")
$(window).load(function() {

    var currency_box = $(".currency-select-box");

    errors = false;

    // Create a Stripe client.
    stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');

    // Create an instance of Elements.
    elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    style = {
        base: {
            color: '#32325d',
            fontFamily: '"avenir-roman", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '14px',
            '::placeholder': {
                color: '#cacdd0'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    card = elements.create('card', {style: style});

    mountCreditCardFields();

    $(document).on('keyup change','.error',function(){
        $(this).removeClass('error');
        $(this).next('.error-message').text("").hide();
        /*if($(this).attr('id') == "check"){
            $("#error-checkTerms").hide();
        }*/
    });
    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();

        $('.error-message').hide();
        errors = false;
        addLoaders();
        if($(".gateway-methods>.active").attr("id") == "stripe"){
            stripe.createToken(card).then(function(result) {
                var name = $('#example1-name').val();
                if (name == '') {
                    $('#example1-name').next('.error-message').text('Enter a valid card holder name.').show();
                    $('#example1-name').addClass('error');
                    errors = true;
                }

                var email = $('#example1-email').val();
                if (email == '') {
                    $('#example1-email').next('.error-message').text('Enter a valid email address.').show();
                    $('#example1-email').addClass('error');
                    errors = true;
                } else {
                    if (!validateEmail(email)){
                        $('#example1-email').next('.error-message').text('Enter a valid email address.').show();
                        $('#example1-email').addClass('error');
                        errors = true;
                    }
                }

                /*var atLeastOneIsChecked = $('#check:checkbox:checked').length > 0;
                if (!atLeastOneIsChecked) {
                    $('#error-checkTerms').show();
                    $('#check').addClass('error');
                    errors = true;
                }*/

                $('#example1-name, #example1-email').parent().removeClass('show-input-loader');
                if (result.error || errors) {
                    // Inform the user if there was an error.
                    /*var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;*/
                    /*$('.success-message').text('Some error occured.');*/
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
                removeLoaders();

            });
        } else {
            var email = $('#example1-email').val();
            if (email == '') {
                $('#example1-email').next('.error-message').text('Enter a valid email address.').show();
                $('#example1-email').addClass('error');
                errors = true;
            } else {
                if (!validateEmail(email)){
                    $('#example1-email').next('.error-message').text('Enter a valid email address.').show();
                    $('#example1-email').addClass('error');
                    errors = true;
                }
            }

            /*var atLeastOneIsChecked = $('#check:checkbox:checked').length > 0;
            if (!atLeastOneIsChecked) {
                $('#error-checkTerms').show();
                errors = true;
            }*/
            if(!errors){
                setTimeout(function(){
                    $('.payment.success').show();
                    $(".payment-details").addClass('submitting');
                    $(".payment-details").removeClass('submitting');
                    $(".payment-details").addClass('submitted');
                    removeLoaders();
                }, 2000);
            } else {
                removeLoaders();
            }
        }
    });

    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);


        $('.payment.success .token').text('Form submitted successflly. Token ID: '+token.id);
        $('.payment.success').show();
        $(".payment-details").addClass('submitting');
        $(".payment-details").removeClass('submitting');
        $(".payment-details").addClass('submitted');

        card.unmount("#card-element");
        card.mount("#card-element");
    }
});

/*
* Validate Email Field on FocusOut
* */
function focusOutEmailField() {
    $('#example1-email').on('blur', function() {
        var email = $(this).val();
        var message = "Enter a valid email address.";
        if (!validateEmail(email)){
            if(email !== ''){
                $(this).addClass('error');
                $(this).siblings('.error-message').html(message);
                $(this).siblings('.error-message').show();
            }else {
                $(this).removeClass('error');
                $(this).siblings('.error-message').hide();
            }
        }else {
            $(this).removeClass('error');
        }
    });
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function createCardElement(){
    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    return card;
}

function mountCreditCardFields(){
    if($(".gateway-methods>.active").attr("id") == "stripe"){
        var creditCardField = $("#creditCardField").html();
        $("#creditCardContainer").html(creditCardField);
        var cardHolderNameField = $("#cardHolderNameField").html();
        $("#cardHolderNameContainer").html(cardHolderNameField);
        $("#creditCardContainer .card-element").attr('id','card-element');
        $("#creditCardContainer .card-errors").attr('id','card-errors');
        $("#creditCardContainer,#cardHolderNameContainer").show();
        $("#submit_btn_title").html($(".gateway-methods>.active").data('name'));
        card = createCardElement();
    }
}

function updateCreditCardFields(gateway_name){
    var active_gateway_name = $(".gateway-methods>.active").data('name');
    $(".payment-details").removeClass('submitted');
    $('.payment.success').hide();
    $("#cardHolderNameContainer,#creditCardContainer").html("");
    $("#creditCardContainer,#cardHolderNameContainer").hide();
    $("#submit_btn_title").html(active_gateway_name);
    $("#agree_with_pay").html(active_gateway_name);
    card.unmount();
    mountCreditCardFields();
}

function addLoaders(){
    $(".payment-btn").addClass("submitting-data");
    $(".email, #card-element, #cardHolderNameContainer .card-holder").addClass('show-input-loader');
}

function removeLoaders(){
    $(".payment-btn").removeClass("submitting-data");
    $(".email, #card-element, #cardHolderNameContainer .card-holder").removeClass('show-input-loader');
}



/*
* Currency change onclick
* */
function onCurrencyChange(e) {
    e.classList.toggle('active');
}
function onCurrencySelect(e) {
    e.classList.toggle('active');
}



