<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9;IE=10;IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Test Cart</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Custom Style -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="./js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./js/bootstrap.min.js"></script>
</head>
<body>
<?php

$packages = [
    [
        'id' => 1,
        'duration' => '1 MONTH PLAN',
        'active' => 0,
        'billing_cycle' => 'monthly',
        'divider' => 1,
        'usd_old_price' => 10.95,
        'usd_price' => 10.95,
        'eur_price' => 10.95,
        'eur_old_price' => 10.95,
        'plan_text' => "1 Month Plan"
    ],
    [
        'id' => 2,
        'duration' => '1 YEAR PLAN',
        'active' => 1,
        'billing_cycle' => 'annually',
        'divider' => 12,
        'usd_old_price' => 10.95,
        'usd_price' => 3.33,
        'eur_price' => 3.33,
        'eur_old_price' => 10.95,
        'plan_text' => "1 Year Plan"
    ],
    [
        'id' => 3,
        'duration' => '3 MONTH PLAN',
        'active' => 0,
        'billing_cycle' => 'triennially',
        'divider' => 3,
        'usd_old_price' => 10.95,
        'usd_price' => 8.00,
        'eur_price' => 8.00,
        'eur_old_price' => 10.95,
        'plan_text' => "3 Month Plan"
    ]
]


?>
<header>
    <img class="img-responsive" src="https://d1v4btv2rwszby.cloudfront.net/public/images/82277_header.png" alt="Header Image">
</header>
<section id="custom-cart" class="custom-cart-wrapper">
    <div class="cart-boxes">
        <div class="container">
            <div class="cart-top-wrapper">
                <h1 class="heading">Experience PureVPN and Remain 100% Secure &amp; Anonymous</h1>
                <p class="para">All plans are covered by a no-hassle 100% money-back guarantee for your first 30 days of
                    service.</p>
                <div class="change-currency">
                    <span class="heading">Change plan</span>
                    <i>Change currency:</i>
                    <span class="currency-select-box" onclick="onCurrencyChange(this)">
                    <b>USD</b>
                    <ul>
                        <li class="active" onclick="onCurrencySelect(this)">USD</li>
                        <!--<li onclick="onCurrencySelect(this)">EUR</li>-->
                    </ul>
                    <!--<select name="currency" id="currency_select" >
                        <option value="USD">USD</option>
                        <option value="EUR">USD</option>
                    </select>-->
                </span>
                </div>
            </div>
            <div class="cart-boxes-wrapper">
                <?php
                foreach ($packages as $pkg) { ?>
                    <section data-pkg-id="<?php echo $pkg['id']; ?>" class="col-xs-4 cart-box <?php echo ($pkg['active'] == 1) ? 'active' : '' ?>">
                        <!-- Cart Template (Section) START -->
                        <div class="cart-box-inner">
                            <div class="cart-box-duration">
                                <span><?php echo $pkg['duration'] ?></span>
                            </div>
                            <div class="cart-box-price">
                                <b><sup>USD</sup><?php echo number_format($pkg['usd_price'], 2) ?></b>
                            </div>
                            <div class="cart-box-month-div">
                                <span>per month</span>
                            </div>
                            <div class="cart-box-dscnt">
                                <?php
                                    $percentage = ( (($pkg['usd_old_price'] * $pkg['divider']) - ($pkg['usd_price'] * $pkg['divider'])) / ($pkg['usd_old_price'] * $pkg['divider']) ) * 100;
                                    if($percentage > 0){ ?>
                                        <span>Save <?php echo ceil($percentage) ?>%</span>
                                    <?php }else{ ?>
                                        <span>No Saving</span>
                                    <?php }
                                ?>
                            </div>
                            <div class="cart-box-btn-wrap">
                                <a href="javascript:void(0);" class="cart-box-btn btn">
                                    <span>Get <?php echo $pkg['duration'] ?></span>
                                </a>
                            </div>
                            <div class="cart-box-btm-text">
                                <span>
                                    31-Day Money-Back Guarantee
                                </span>
                            </div>

                        </div>
                    </section><!-- Cart Template (Section) END -->
                <?php }

                ?>
            </div>
        </div>
    </div>
    <div class="gateway-wrapper">
        <div class="container">
            <div class="col-sm-7 col-left">
                <div class="top">
                    <span class="first">Select Payment</span>
                    <span class="second"><i class="icon encrypt-icon"></i>Encrypted and Secured</span>
                    <img class="pull-right" src="https://d1v4btv2rwszby.cloudfront.net/public/images/82277_ic-mcafee.png" alt="mcafee-icon">
                </div>
                <div class="gateway-methods">
                    <div class="col-sm-4 gateway-method active" id="stripe" data-name="Credit Card">
                        <div class="gateway-method-inr">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-sm-4 gateway-method" id="paypal" data-name="Paypal">
                        <div class="gateway-method-inr">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-sm-4 gateway-method" id="crypto" data-name="Crypto Currency">
                        <div class="gateway-method-inr">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-sm-4 gateway-method" id="alipay" data-name="Alipay">
                        <div class="gateway-method-inr">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-sm-4 gateway-method" id="bluesnap" data-name="BlueSnap">
                        <div class="gateway-method-inr">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-sm-4 gateway-method" id="giftcard" data-name="Gift Cards">
                        <div class="gateway-method-inr">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="payment-details">
                    <?php
                        foreach ($packages as $pkg) {
                            if($pkg['active'] === 1){ ?>
                                <div class="payment-top">
                                    <div class="payment-plan">
                                        <span class="pull-left">
                                            PureVPN <?php echo $pkg['plan_text'] ?>
                                        </span>
                                        <span class="pull-right">
                                            USD <?php echo number_format(($pkg['usd_old_price'] * $pkg['divider']),2) ?>
                                        </span>
                                    </div>
                                    <?php
                                        $percentage = ( (($pkg['usd_old_price'] * $pkg['divider']) - ($pkg['usd_price'] * $pkg['divider'])) / ($pkg['usd_old_price'] * $pkg['divider']) ) * 100;
                                        if($percentage > 0){ ?>
                                            <div class="payment-plan-discount">
                                                <span class="pull-left">
                                                    Discount <?php echo ceil($percentage) ?>%
                                                </span>
                                                        <span class="pull-right">
                                                    - USD <?php echo (($pkg['usd_old_price'] * $pkg['divider']) - ($pkg['usd_price'] * $pkg['divider'])) ?>
                                                </span>
                                            </div>
                                    <?php } ?>
                                    <div class="payment-plan-discount-coupon">
                                        <span class="pull-left">

                                        </span>
                                        <span class="pull-right">

                                        </span>
                                    </div>
                                </div>
                                <div class="payment-total">
                                    <span class="pull-left">
                                        Total
                                    </span>
                                    <span class="pull-right">
                                        USD <?php echo ($pkg['usd_price'] * $pkg['divider']) ?>
                                    </span>
                                </div>
                                <div class="payment-coupon">
                                    <span class="pull-left">
                                        Got coupon?
                                    </span>
                                    <div class="clearfix"></div>
                                    <!-- add Class when coupon is valid 'coupon-applied'-->
                                    <div class="payment-coupon-box">
                                        <input type="text" class="payment-coupon-inpt" name="coupon-input" placeholder="Enter coupon">
                                        <button class="btn payment-coupon-btn disabled">Apply</button>
                                        <button class="btn payment-coupon-cancel"><i class="close-icon">x</i></button>
                                    </div>
                                    <div class="payment-coupon-box-message">
                                        <span>Coupon code applied</span>
                                    </div>
                                </div>
                                <div class="payment-form">


                                    <div class="success-message"></div>


                                    <div class="cell example example1" id="example-1">
                                        <form method="post" id="payment-form">
                                            <div class="form-group" id="cardHolderNameContainer" style="display:none;">

                                            </div>
                                            <div class="form-group">
                                                <div class="email">
                                                    <input id="example1-email" data-tid="elements_examples.form.email_placeholder" type="email" class="form-control" placeholder="Enter your email address" name="email"  autocomplete="email">
                                                    <p class="error-message">Enter a valid email</p>
                                                </div>
                                            </div>
                                            <div class="form-group" id="creditCardContainer" style="display:none;">
                                                <div id="card-element" class="card-element"></div>
                                            </div>

                                            <div class="payment-button">
                                                <button type="submit" value="Login" class="payment-btn col-sm-7" data-tid="elements_examples.form.pay_button">
                                                    <span>Pay With <b id="submit_btn_title">Credit Card</b></span>
                                                    <div class="btn-loader">
                                                        <div class="dot dot1"></div>
                                                        <div class="dot dot2"></div>
                                                        <div class="dot dot3"></div>
                                                    </div>
                                                </button>
                                                <div class="guarantee-box">
                                                    <p><span class="gur-icon icon-refund"></span><span class="gur-text">30-day money-back guarantee</span></p>
                                                    <p><span class="gur-icon icon-secure"></span><span class="gur-text">Encrypted and secure payments</span></p>
                                                </div>
                                            </div>
                                            <div class="form-group wt-checkTerms">
                                                <span>
                                                    By clicking Pay with <i id="agree_with_pay">Credit Card</i>, you agree to our <a href="https://www.purevpn.com/term.php" target="_blank">Terms and Conditions</a>.
                                                    Learn how we collect, use and share your data in our <a href="https://www.purevpn.com/privacy-policy.php" target="_blank">Privacy Policy</a>.
                                                </span>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <div class="payment success" style="display:none">
                                    <div class="icon">
                                        <svg width="84px" height="84px" viewBox="0 0 84 84" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
                                            <circle class="border" cx="42" cy="42" r="40" stroke-linecap="round" stroke-width="4" stroke="#000" fill="none"></circle>
                                            <path class="checkmark" stroke-linecap="round" stroke-linejoin="round" d="M23.375 42.5488281 36.8840688 56.0578969 64.891932 28.0500338" stroke-width="4" stroke="#000" fill="none"></path>
                                        </svg>
                                    </div>
                                    <h3 class="title" data-tid="elements_examples.success.title">Payment successful</h3>
                                    <p class="message"><span data-tid="elements_examples.success.message">Thanks for trying PVPN cart demo. (No money was charged in the process).</span></p>
                                </div>
                             <?php }

                        }
                    ?>
                </div>
            </div>
            <div class="col-sm-5 col-right">
                <div class="get-acct-details">
                    <img class="img-responsive" src="https://d1v4btv2rwszby.cloudfront.net/public/images/82277_ic-31-day.png" alt="31-Day-Image">
                    <p class="heading">PureVPN plan includes:</p>
                    <ul>
                        <li>
                            <span class="icon-check"></span>24/7 customer support
                        </li>
                        <li>
                            <span class="icon-check"></span> <i class="devices-text">Apps for</i>
                            <span class="icon-apps icon-win"></span>
                            <span class="icon-apps icon-mac"></span>
                            <span class="icon-apps icon-andrd"></span>
                            <span class="icon-apps icon-ios"></span>
                            <span class="icon-apps icon-linux"></span>
                        </li>
                        <li>
                            <span class="icon-check"></span>5-Multi login so you can use one account on 5 devices at the same time.
                        </li>
                        <li>
                            <span class="icon-check"></span>Enterprise-grade security
                        </li>

                        <li>
                            <span class="icon-check"></span>2000+ secure servers in 140+ countries
                        </li>
                    </ul>
                </div>
                <img class="img-responsive trustpilot-image" src="https://d1v4btv2rwszby.cloudfront.net/public/images/82277_img-trustpilot.png" alt="Trust-Pilot-Image">
            </div>
        </div>
    </div>
    <div id="creditCardField" style="display:none">
        <div class="row">
            <div class="col-md-12">
                <div class="card-element">
                    <!-- A Stripe Element will be inserted here. -->
                </div>
                <div class="card-errors" role="alert"></div>
            </div>
        </div>
    </div>
    <div id="cardHolderNameField" style="display:none">
        <div class="card-holder">
            <input type="text" class="form-control" name="name" id="example1-name" data-tid="elements_examples.form.name_placeholder" placeholder="Card holder name"  autocomplete="name">
            <p class="error-message">Enter a valid card holder name</p>
        </div>
    </div>
</section>

<script src="https://js.stripe.com/v3/"></script>
<script src="./js/custom.js"></script>
</body>
</html>